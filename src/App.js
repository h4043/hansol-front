import { createGlobalStyle } from "styled-components";
import Logo from "./Logo";
import Nav from "./Nav";
import RNRList from "./RNRList";
import RNRTemplate from "./RNRTemplate";

const GlobalStyle = createGlobalStyle`
  body {
    background: #f3f3f3;
    width: 85%;
    margin: 0 auto;
  }
  
`;

function App() {
  return (
    <>
      <GlobalStyle />
      <Logo></Logo>
      <Nav></Nav>
      <RNRTemplate>
        <RNRList></RNRList>
      </RNRTemplate>
    </>
  );
}

export default App;
