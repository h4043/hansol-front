import styled from "styled-components";
import React, { useState, useEffect } from "react";
import axios from "axios";

const RNRsBlock = styled.div`
  overflow: hidden;
  font-size: 1em;
  width: 100%;

  table {
    width: 100%;
    border-collapse: collapse;
  }

  td,
  th {
    border-bottom: 1px solid #dadada;
    text-align: left;
    padding: 10px;
  }

  tr:nth-child(even) {
    background-color: #eeeeee;
  }
`;

function RNRs() {
  const [rnrs, setRnrs] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchRnrs = async () => {
      try {
        // 요청이 시작 할 때에는 error 와 users 를 초기화하고
        setError(null);
        setRnrs(null);
        // loading 상태를 true 로 바꿉니다.
        setLoading(true);
        const response = await axios.get("http://localhost:8080/rnr/list");
        setRnrs(response.data);
      } catch (e) {
        setError(e);
      }
      setLoading(false);
    };

    fetchRnrs();
  }, []);

  if (loading) return <div>로딩중..</div>;
  if (error) return <div>에러가 발생했습니다</div>;
  if (!rnrs) return null;

  return (
    <RNRsBlock>
      <table>
        <thead>
          <tr>
            <th>번호</th>
            <th>업무코드</th>
            <th>업무명</th>
            <th>회사</th>
            <th>담당자</th>
            <th>직급</th>
            <th>종류</th>
            <th>전화번호</th>
          </tr>
        </thead>
        <tbody>
          {rnrs.map((rnr) => (
            <tr key={rnr.rno}>
              <td>{rnr.rno}</td>
              <td>{rnr.task.tno}</td>
              <td>{rnr.task.tname}</td>
              <td>{rnr.companyList.map((company) => company.cname + " ")}</td>
              <td>{rnr.employee.name}</td>
              <td>{rnr.employee.position}</td>
              <td>{rnr.kind}</td>
              <td>{rnr.employee.tel}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </RNRsBlock>
  );
}

export default RNRs;
