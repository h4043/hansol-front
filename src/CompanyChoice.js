import React, { useState, useEffect } from "react";
import axios from "axios";

function CompanyChoice({ getCompany }) {
  const [companys, setCompany] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const [checkedInputs, setCheckedInputs] = useState([]);

  const handleCheck = (checked, id) => {
    if (checked) {
      setCheckedInputs([...checkedInputs, id]);
    } else {
      // 체크 해제
      setCheckedInputs(checked.filter((el) => el !== id));
    }
  };

  getCompany(checkedInputs);

  useEffect(() => {
    const fetchCompanys = async () => {
      try {
        setError(null);
        setCompany(null);
        setLoading(true);
        const response = await axios.get(
          "http://localhost:8080/rnr/company/list"
        );
        setCompany(response.data);
      } catch (e) {
        setError(e);
      }
      setLoading(false);
    };

    fetchCompanys();
  }, []);

  if (loading) return <option>로딩중..</option>;
  if (error) return <option>에러가 발생했습니다</option>;
  if (!companys) return null;

  return (
    <>
      {companys.map((company) => (
        <label key={company.cno}>
          <input
            type="checkbox"
            id={company.cno}
            name="cnoList"
            value={company.cno}
            onChange={(e) => {
              handleCheck(e.currentTarget.checked, company.cno);
            }}
            checked={checkedInputs.includes(company.cno) ? true : false}
          />
          {company.cname}
        </label>
      ))}
    </>
  );
}

export default CompanyChoice;
