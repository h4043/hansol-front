import { useState, useEffect } from "react";
import CompanyChoice from "./CompanyChoice";
import EmployeeSelect from "./EmployeeSelect";
import TaskSelect from "./TaskSelect";
import axios from "axios";

function Modal() {
  const [open, setOpen] = useState(false);
  const onToggle = () => setOpen(!open);

  const [task, setTask] = useState("");
  const [employee, setEmployee] = useState("");
  const [kind, setKind] = useState("");
  const [company, setCompany] = useState("");

  const getTask = (data) => {
    setTask(data);
  };
  const getEmployee = (data) => {
    setEmployee(data);
  };

  const getKind = (e) => {
    setKind(e.target.value);
  };

  const getCompany = (data) => {
    setCompany(data);
  };

  const onSubmit = () => {
    console.log(task);
    console.log(employee);
    console.log(kind);
    console.log(company);
    axios
      .post(
        "http://localhost:8080/rnr/register",
        JSON.stringify({
          kind: kind,
          tno: task,
          eno: employee,
          cnoList: company,
        }),
        {
          headers: { "Content-Type": `application/json` },
        }
      )
      .then((res) => {
        console.log(res);
        onToggle();
      });
  };

  return (
    <>
      {open && (
        <div className="modalLayout">
          <div className="modal">
            <div className="close" onClick={onToggle}>
              X
            </div>
            <h2>등록하기</h2>
            <form>
              <label className="modalLabel">
                업무
                <TaskSelect getTask={getTask} />
              </label>
              <label className="modalLabel">
                회사
                <CompanyChoice getCompany={getCompany} />
              </label>
              <label className="modalLabel">
                담당
                <EmployeeSelect getEmployee={getEmployee} />
              </label>
              <label className="modalLabel">
                종류
                <input name="kind" onChange={getKind} value={kind} />
              </label>
              <button type="button" onClick={onSubmit}>
                등록하기
              </button>
            </form>
          </div>
        </div>
      )}
      <button className="openModal" onClick={onToggle} open={open}>
        등록하기
      </button>
    </>
  );
}

export default Modal;
