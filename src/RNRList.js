import styled from "styled-components";
import RNRs from "./RNRs";

const RNRListBlock = styled.div`
  flex: 1;
  padding: 20px 0px;
  padding-bottom: 48px;
  /* overflow-y: auto; */
`;

function RNRList() {
  return (
    <RNRListBlock>
      <RNRs />
    </RNRListBlock>
  );
}

export default RNRList;
