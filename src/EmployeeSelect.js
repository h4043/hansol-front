import React, { useState, useEffect } from "react";
import axios from "axios";

function EmployeeSelect({ getEmployee }) {
  const [employees, setEmployees] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const [selected, setSelected] = useState("");

  const handleSelect = (e) => {
    setSelected(e.target.value);
  };

  getEmployee(selected);

  useEffect(() => {
    const fetchEmployees = async () => {
      try {
        setError(null);
        setEmployees(null);
        setLoading(true);
        const response = await axios.get(
          "http://localhost:8080/rnr/employee/list"
        );
        setEmployees(response.data);
      } catch (e) {
        setError(e);
      }
      setLoading(false);
    };

    fetchEmployees();
  }, []);

  if (loading) return <option>로딩중..</option>;
  if (error) return <option>에러가 발생했습니다</option>;
  if (!employees) return null;

  return (
    <select name="eno" onChange={handleSelect} value={selected}>
      <option>--선택--</option>
      {employees.map((employee) => (
        <option key={employee.eno} value={employee.eno}>
          {employee.eno} ({employee.name} {employee.position})
        </option>
      ))}
    </select>
  );
}

export default EmployeeSelect;
