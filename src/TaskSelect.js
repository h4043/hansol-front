import React, { useState, useEffect } from "react";
import axios from "axios";

function TaskSelect({ getTask }) {
  const [tasks, setTasks] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const [selected, setSelected] = useState("");

  const handleSelect = (e) => {
    setSelected(e.target.value);
  };

  getTask(selected);

  useEffect(() => {
    const fetchTasks = async () => {
      try {
        setError(null);
        setTasks(null);
        setLoading(true);
        const response = await axios.get("http://localhost:8080/rnr/task/list");
        setTasks(response.data);
      } catch (e) {
        setError(e);
      }
      setLoading(false);
    };

    fetchTasks();
  }, []);

  if (loading) return <option>로딩중..</option>;
  if (error) return <option>에러가 발생했습니다</option>;
  if (!tasks) return null;

  return (
    <select name="tno" id="tno" onChange={handleSelect} value={selected}>
      <option>--선택--</option>
      {tasks.map((task) => (
        <option key={task.tno} value={task.tno}>
          {task.tno} ({task.tname})
        </option>
      ))}
    </select>
  );
}

export default TaskSelect;
