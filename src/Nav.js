import styled from "styled-components";
import Modal from "./Modal";

const NavStyle = styled.div`
  font-size: 22px;
  margin-top: 20px;
  display: flex;
  justify-content: space-between;

  .openModal {
    width: 10%;
    font-size: 17px;
    line-height: 2em;
    color: white;
    border: none;
    border-radius: 5px;
    background-color: #3993dd;
    padding: 5px 0;
  }

  .openModal:hover {
    background-color: #6ca7d8;
    cursor: grab;
  }

  .modalLayout {
    @include flex-center;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #00000075;
    z-index: 10000;
  }
  .modal {
    position: absolute;
    width: 600px;
    height: 700px;
    margin: 0 auto;
    background-color: white;
    padding: 5px 30px;
    border-radius: 10px;
    left: 35%;
    top: 10%;
  }
  .modalLabel {
    display: inline-block;
    width: 100%;
    font-size: 15px;
    margin-bottom: 10px;
  }
  input,
  select {
    margin-left: 15px;
  }
  .close {
    width: 100%;
    text-align: right;
    font-weight: bold;
  }
`;
function Nav() {
  return (
    <NavStyle>
      R&R관리
      <Modal></Modal>
    </NavStyle>
  );
}

export default Nav;
