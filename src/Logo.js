import styled from "styled-components";

const LogoStyle = styled.div`
  font-size: 45px;
  font-weight: bold;
  color: #3993dd;
  margin-top: 20px;
`;
function Logo() {
  return <LogoStyle>Hansol</LogoStyle>;
}

export default Logo;
