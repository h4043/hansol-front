import styled from "styled-components";

const RNRTemplateBlock = styled.div`
  width: 100%;
  /* height: 768px; */

  position: relative;
  background: white;
  border-radius: 16px;
  box-shadow: 0 0 8px rgba(0, 0, 0, 0.04);

  margin: 0 auto;
  margin-top: 15px;
  margin-bottom: 32px;

  display: flex;
  flex-direction: column; // 위에서 아래로

  button {
    width: 10%;
    font-size: 1em;
    line-height: 2em;
    color: white;
    border: none;
    border-radius: 5px;
    background-color: #3993dd;
    padding: 5px 0;
  }
`;
function RNRTemplate({ children }) {
  return <RNRTemplateBlock>{children}</RNRTemplateBlock>;
}

export default RNRTemplate;
